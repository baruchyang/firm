<?php

namespace app\controllers;
use Yii;
use app\models\Person;
use app\models\Department;
use app\models\DepartmentPerson;
use yii\web\Controller;



class MainController extends Controller
{
    public function actionIndex()
    {

        $departments = Department::find()->orderBy('title ASC')->all();

        $intersections = array();
        $intersectionResult = DepartmentPerson::find()->all();
        if($intersectionResult){
            foreach($intersectionResult as $intersection){
                if(!isset($intersections[$intersection['person_id']]))$intersections[$intersection['person_id']] = array();
                $intersections[$intersection['person_id']][] = $intersection['department_id'];
            }
        }

        $persons = Person::find()
            ->select(['id','surname','name'])
            ->orderBy(['surname' => 'ASC', 'name' => 'ASC'])
            ->all();

        return $this->render('index',[
                'persons' => $persons,
                'departments' => $departments,
                'intersections' => $intersections,

                'columnCounts' => count($departments)
        ]);
    }

}
