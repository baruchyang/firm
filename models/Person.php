<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%person}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $patronymic
 * @property string $sex
 * @property integer $salary
 * @property array $departments
 *
 * @property DepartmentPerson[] $departmentPeople
 */
class Person extends \yii\db\ActiveRecord
{
    protected $departments = [];
    /**
     * @param array $departments
     */


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%person}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'message' => 'Ввеите имя сотрудника'],
            [['surname'], 'required', 'message' => 'Ввеите фамилию сотрудника'],
            [['sex'], 'string'],
            [['salary'], 'integer'],
            [['name', 'surname', 'patronymic'], 'string', 'max' => 50],
            [['departments'], 'required', 'message' => 'Укажите отдел '],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'patronymic' => 'Отчество',
            'sex' => 'Пол',
            'salary' => 'Зарплата',
            'departments' => 'Отделы'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentPeople()
    {
        return $this->hasMany(DepartmentPerson::className(), ['person_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getDepartments()
    {
//        return [];
        if (!count($this->departments)){
            $query = DepartmentPerson::find()
                ->select('department_id')
                ->where( ['person_id' => $this->id])
                ->asArray()
                ->all();
//            var_dump($this->departments);
            $this->departments = ArrayHelper::getColumn($query, 'department_id');
            return $this->departments;
         }
        else
            return $this->departments;
    }


    /**
     * @param array $departments
     */
    public function setDepartments($departments)
    {
        $this->departments = $departments;
    }


    public function afterSave($insert, $changedAttributes)
    {
//        var_dump($insert);
//        var_dump($changedAttributes);
//        var_dump(Yii::$app->request->post());

        $this->updateDepartments();
        parent::afterSave($insert, $changedAttributes);
    }

    protected function updateDepartments()
    {
//        var_dump($this->departments);
        DepartmentPerson::deleteAll(array('person_id' => $this->id));
        if (is_array($this->departments))
            foreach ($this->departments as $id) {
                $departmentPerson = new DepartmentPerson();
                $departmentPerson->person_id = $this->id;
                $departmentPerson->department_id = $id;
                $departmentPerson->save();
            }
    }

    public function getFio(){
        return $this->surname.' '.$this->name.' '.$this->patronymic;
    }


    public function getFormatingSalary(){
        return ($this->salary != 0) ? number_format($this->salary, 0, ' ', ' ') . ' рублей' : 'Не указана';
    }

    public function getStringPersonDepartmentTitles(){

        $query = Department::find()
                ->where(['in','id', $this->getDepartments() ]) //
                ->all();
        if(!$query) return '';
        $titles = ArrayHelper::getColumn($query, 'title');

        return implode(', ',$titles);

    }

}
