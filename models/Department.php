<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%department}}".
 *
 * @property integer $id
 * @property string $title
 *
 * @property DepartmentPerson[] $departmentPeople
 */
class Department extends \yii\db\ActiveRecord
{
    public $countPerson;
    public $maxSalary;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%department}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required', 'message' => 'Ввеите название отдела'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'unique', 'message' => 'Отдел с таким название уже существует'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название отдела',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentPeople()
    {
        return $this->hasMany(DepartmentPerson::className(), ['department_id' => 'id']);
    }

    public function getFormatingSalary(){
        return ($this->maxSalary != 0) ? number_format($this->maxSalary, 0, ' ', ' ') . ' рублей' : ' - ';
    }
}
