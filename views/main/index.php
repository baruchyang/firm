<?php
/* @var $this yii\web\View */
$this->title = 'Главная страница';
?>
    <table id="main-table" class="table table-hover table-striped table-bordered">
        <thead>
            <th class="col-md-2">&nbsp;</th>
            <?php foreach($departments as $department):?>
                <th><?=$department->title?></th>
            <?php endforeach ?>
        </thead>
        <?php foreach($persons as $person):?>
            <tr>
                <td class="name"><?=$person->surname.' '.$person->name?></td>
                <?php foreach($departments as $department):?>
                    <td>
                        <?=((!empty($intersections[$person->id])) && (array_search($department->id,$intersections[$person->id]) !== false)) ? '✓' : '&nbsp;'?>
                    </td>
                <?php endforeach ?>
            </tr>

        <?php endforeach ?>
    </table>

