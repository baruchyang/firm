<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php //$this->head() ?>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="shortcut icon" href="/favicon.ico">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/styles.css" />

    <script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>

    <!-- github acitivity css -->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/octicons/2.0.2/octicons.min.css">
    <link rel="stylesheet" href="http://caseyscarborough.github.io/github-activity/github-activity-0.1.0.min.css">


</head>
<body>
<?php $this->beginBody() ?>
<header class="header">
    <div class="container">
        <?= Nav::widget([
            'options' => ['class' => 'nav nav-pills pull-right'],
            'items' => [
                ['label' => 'Главная', 'url' => ['/']],
                ['label' => 'Отделы', 'url' => ['/department/index']],
                ['label' => 'Сотрудники', 'url' => ['person/index']],
            ],
        ]);
        ?>

    </div><!--//container-->
</header><!--//header-->

<div class="container sections-wrapper">
    <div class="row">
        <div class="primary col-md-12 col-sm-12 col-xs-12">
            <?= Breadcrumbs::widget([
                'homeLink' => ['label' => 'Главная', 'url' => '/'],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <section class="about section">
                <div class="section-inner">
                    <div class="content">
                        <?= $content ?>
                    </div><!--//content-->
                </div><!--//section-inner-->
            </section><!--//section-->


        </div><!--//primary-->

    </div><!--//row-->
</div><!--//masonry-->

<!-- ******FOOTER****** -->
<footer class="footer">
    <div class="container text-center">
        <small class="copyright">Powered by Baruchyan G.</small>
    </div><!--//container-->
</footer><!--//footer-->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
