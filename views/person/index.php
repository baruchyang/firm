<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сотрудники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить сотруника', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-hover table-striped table-bordered'
        ],
        'columns' => [
            'surname', 'name', 'patronymic', 'sex',
            [
                'attribute' => 'salary',
                'content' => function($data){
                    return $data->getFormatingSalary();
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия',
                'headerOptions' =>['class' => 'col-md-1'],
                'template' => '{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{delete}',
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
