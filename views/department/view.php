<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\Url;



/* @var $this yii\web\View */
/* @var $model app\models\Department */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Отделы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

        <?php if(!$persons){//удаление только для пустых отделов

            echo Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
            'confirm' => 'Вы действительно хотите удалить данный отдел?',
            'method' => 'post',
            ],
            ]);

        }?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
        ],
    ]) ?>

    <?php if($persons){?>

      <h2>Сотрудники отдела</h2>
        <ul>
            <?php foreach($persons as $person):?>
                <li><a href="<?=Url::toRoute(['person/view', 'id' => $person->id]);?>"><?=$person->getFio()?></a></li>
            <?php endforeach?>
        </ul>

    <?php } ?>

</div>
