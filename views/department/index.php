<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отделы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить отдел', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-hover table-striped table-bordered'
        ],
        'columns' => [
            [
                'attribute'=>'title',
                'label'=>'Отдел',
            ],
            [
                'attribute'=>'countPerson',
                'header'=>'Сотрудники',
                'headerOptions' =>['class' => 'col-md-2'],
//                'contentOptions' =>['class' => 'info']
            ],
            [
                'attribute'=>'maxSalary',
                'header'=>'Макс. зарплата',
                'headerOptions' =>['class' => 'col-md-2'],
//                'contentOptions' =>['class' => 'info'],
                'content' => function($data){
                    return $data->getFormatingSalary();
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия',
                'headerOptions' =>['class' => 'col-md-1'],
                'template' => '{view}&nbsp;&nbsp;{update}'

            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
